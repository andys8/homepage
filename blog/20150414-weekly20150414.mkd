---
author: thoughtpolice
title: "GHC Weekly News - 2015/04/14"
date: 2015-04-14T17:44:28
tags: ghc news
---

Hi \*,

It's been a few weeks since the last news bulletin - your editor
apologizes about that. It's actually been a relatively slow few weeks
here too, and busy-ness outside of GHC has attracted some of my
attention. Despite that, GHC 7.10.1 was released, a new HP alpha is
out, and things are moving along smoothly. Now that the release is
done, things are quitely moving along in `HEAD` - with people
committing code with reckless abandon, of course.

This week, GHC HQ met up, but it's been very light since the 7.10.1
release. Currently there isn't anything pushing us to do a 7.10.2
release at least for a few more weeks it looks like - but see below.

  - We puzzled a bit about the release status of 7.10.2, and thought: it's only holding up people who are depending on it. So, who's depending on it, and what do they need fixed? See below for more.

  - We also talked a bit about performance - it seems the compiler has been getting much slower over time since the 7.8.x release series, and it's time to reign it in. Austin will be spending his week investigating a bit of this, and the causes.

## 7.10.2 Status

So, you may be wondering when the 7.10.2 release is. The trick is it happens when you tell us it should happen!

So far, taking a look at milestone:7.10.2, we've fixed about half the bugs we currently have marked down to fix. But we're probably going to punt some of those - and we're not sure all the ones that are there should be.

So this is a call: **If you need something to be fixed during 7.10.2, please file a ticket, set the milestone, and alert us**. The sooner the better, because it'll inform us as to when we should release. Emailing `ghc-devs@haskell.org` is also a sure-fire way to get our attention.

And remember: you can always find out the latest about the next
release at the Status page (in this case, for 7.10.2) -
<https://ghc.haskell.org/trac/ghc/wiki/Status/GHC-7.10.2>

## Call for help: DocBook to AsciiDoc

The GHC team needs some help. A few months ago, we put out a poll to convert our DocBook-based manual to AsciiDoc.

The poll had a mostly lukewarm reception, with the idea that it will A) make life easier for people who frequently modify the users guide, and B) make life easier for people who add things irregularly, as a lower barrier to entry.

It looks like we still **want** to do this - but alas, many of us don't have time!

So, we're asking the public: Is anyone willing to step up and help here? For example, it may be possible to get a long ways with just `pandoc`, but we need someone to finish it - and in return, we'll help along the way!

## List chatter

 - Austin Seipp announced GHC
   7.10.1. <https://mail.haskell.org/pipermail/ghc-devs/2015-March/008700.html>

 - Mark Lentczner announced an alpha Haskell Platform
   release. <https://mail.haskell.org/pipermail/ghc-devs/2015-March/008724.html>

 - David Macek announced MSYS2 packages for GHC on Windows, and also
   asked for some help with continuous windows building - Windows
   hackers should help out! <https://mail.haskell.org/pipermail/ghc-devs/2015-March/008735.html>

 - Jan Stolarek reports about increased memory usage with GHC 7.10.1.
   <https://mail.haskell.org/pipermail/ghc-devs/2015-April/008751.html>

 - Thomas Miedema chimed into a thread started by Karel Gardas about
   better parallelizing the GHC build - and hopefully we can get
   something good out of
   it. <https://mail.haskell.org/pipermail/ghc-devs/2015-April/008749.html>

 - Austin Seipp made a call for help on working on and improving the
   GHC homepage, and luckily Sergey Bushnyak answered the call and
   has helped out!
   <https://mail.haskell.org/pipermail/ghc-devs/2015-April/008762.html>

 - Ozgun Ataman kicked off a thread about slower compilation times,
   with some nasty numbers. It's becoming more clear compiler
   performance should be a priority for 7.12, and we've let some
   things slip away from us:
   <https://mail.haskell.org/pipermail/ghc-devs/2015-April/008766.html>

 - A GHC user, Dave, asked the list about some questions with Cross
   Compilation, as he's attempting to get GHC to work natively inside
   the Open Embedded build environment. Unfortunately, things haven't
   been going well so far, and any input from enterprising hackers is
   appreciated:
   <https://mail.haskell.org/pipermail/ghc-devs/2015-April/008774.html>

 - Dan Aloni has started a discussion about improving GHC's error
   messages, spurred by a popular blog post he wrote and posted on
   Reddit about some Happy/GHC improvements he's made. This is a
   difficult area (error messages in general are hard) to work on, so
   thanks to Dan for helping!
   <https://mail.haskell.org/pipermail/ghc-devs/2015-April/008778.html>

 - Simon Peyton Jones started a discussion about
   `GeneralizedNewtypeDeriving` and Safe Haskell, in particular,
   whatever the current status, our documentation doesn't accurately
   reflect it! Perhaps someone could help out writing the
   documentation based on the current status quo?
   <https://mail.haskell.org/pipermail/ghc-devs/2015-April/008783.html>

 - Tamar Christina started a thread about replacing `ghc-split`, an
   old Perl script inside GHC, but he wanted to know: what do we do
   about a regex replacement? Mikhail Glushenkov spoke up about a
   similar decision the LLVM developers used: to use the OpenBSD
   regex implementation. <https://mail.haskell.org/pipermail/ghc-devs/2015-April/008785.html>

 - Alan Zimmerman has posted several questions and threads about the
   parser and the status of API annotations, which he's been
   furiously working on now that GHC 7.10 is being used on
   Hackage. Interested onlookers could learn a thing or two!
   <https://mail.haskell.org/pipermail/ghc-devs/2015-April/008782.html>
   &
   <https://mail.haskell.org/pipermail/ghc-devs/2015-April/008787.html>
   &
   <https://mail.haskell.org/pipermail/ghc-devs/2015-April/008794.html>

 - Gabor Greif has a question about some seemingly strange behavior
   regarding the interaction between poly-kinded `data` types and
   overlapping instances. Richard sez: this behavior is
   expected. <https://mail.haskell.org/pipermail/ghc-devs/2015-April/008804.html>

## Noteworthy commits

 - Commit de1160be047790afde4ec76de0a81ba3be0c73fa - refactor the
   story around switch cases (with a code-size improvement)

 - Commit 995e8c1c8692b60c907c7d2ccea179d52ca8e69e - drop old
   `integer-gmp-0.5` source code.

 - Commit 59f7a7b6091e9c0564f3f370d09398d8c9cd8ad5 - Restore unwind
   information generation (fixes DWARF generation)

 - Commit 9f0f99fd41ff82cc223d3b682703e508efb564d2 - Fix an old bug
   in the demand analyzer (with some nice compiler performance
   boosts).

 - Commit a7524eaed33324e2155c47d4a705bef1d70a2b5b - Support for
   multiple signature files in scope (Backpack).

## Closed tickets

#10222, #10219, #8057, #10226, #10220, #9723, #10230, #10208, #10236, #10213, #10231, #10240, #10243, #10237, #10224, #8811, #10197, #10252, #9958, #10253, #8248, #10207, #10214, #9964, #10194, #10251, #10188, #10257, #10247, #10247, #9160, #10259, #9965, #10265, #10264, #10286, #10282, #10290, #10291, #10300, #9929, #8276, #10218, #10148, #10232, #10274, #10275, #10195, and #10233.
