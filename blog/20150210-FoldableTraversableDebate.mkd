---
author: simonpj
title: "GHC 7.10 Prelude: we need your opinion"
date: 2015-02-10T15:52:53
tags: 
---

This post asks for your help in deciding how to proceed with some Prelude changes in GHC 7.10.  Please read on, but all the info is also at the survey link, here: <http://goo.gl/forms/XP1W2JdfpX>.   Deadline is 21 Feb 2015.

The Core Libraries Committee (CLC) is responsible for developing the core libraries that ship with GHC. This is an important but painstaking task, and we owe the CLC a big vote of thanks for taking it on.

For over a year the CLC has been working on integrating the Foldable and Traversable classes (shipped in base in GHC 7.8) into the core libraries, and into the Prelude in particular. Detailed planning for GHC 7.10 started in the autumn of 2014, and the CLC went ahead with this integration. 

Then we had a failure of communication.  As these changes affect the Prelude, which is in scope for all users of Haskell, these changes should be held to a higher bar than the regular libraries@ review process.  However, the Foldable/Traversable changes were not particularly well signposted. Many people have only recently woken up to them, and some have objected (both in principle and detail).

This is an extremely unfortunate situation. On the one hand we are at RC2 for GHC 7.10, so library authors have invested effort in updating their libraries to the new Prelude. On the other, altering the Prelude is in effect altering the language, something we take pretty seriously. We should have had this debate back in 2014, but here we are, and it is unproductive to argue about whose fault it is. We all share responsibility.
We need to decide what to do now. A small group of us met by Skype and we've decided to do this:

 * Push back GHC 7.10's release by at least a month, to late March.  This delay also gives us breathing space to address an unrelated show-stopping bug, Trac #9858.

 * Invite input from the Haskell community on which of two approaches to adopt ([this survey](http://goo.gl/forms/XP1W2JdfpX)).  The main questions revolve around impact on the Haskell ecosystem (commercial applications, teaching, libraries, etc etc), so we want to ask your opinion rather than guess it.

 * Ask Simon Marlow and Simon Peyton Jones to decide which approach to follow for GHC 7.10. 

Wiki pages have been created summarizing these two primary alternatives, including many more points and counter-points and technical details: 
 * [Overall summary](https://ghc.haskell.org/trac/ghc/wiki/Prelude710)
 * [Details of Plan List](https://ghc.haskell.org/trac/ghc/wiki/Prelude710/List)
 * [Details of Plan FTP](https://ghc.haskell.org/trac/ghc/wiki/Prelude710/FTP)

This survey invites your input on which plan we should follow. Would you please
 * Read the details of the alternative plans on the three wiki pages above
 * Add your response to [the survey](http://goo.gl/forms/XP1W2JdfpX)

Please do read the background.  Well-informed responses will help.  Thank you!

DEADLINE: 21 February 2015

Simon PJ
